function estBissextile(annee) {
    // Une année est bissextile si elle est divisible par 4, mais pas par 100, sauf si elle est également divisible par 400
    if (annee % 4 === 0) {
      if (annee % 100 === 0) {
        if (annee % 400 === 0) {
          return true; // Divisible par 400, donc c'est une année bissextile
        } else {
          return false; // Divisible par 100 mais pas par 400, donc ce n'est pas une année bissextile
        }
      } else {
        return true; // Divisible par 4 mais pas par 100, donc c'est une année bissextile
      }
    } else {
      return false; // Pas divisible par 4, donc ce n'est pas une année bissextile
    }
  }