// Demander à l'utilisateur la température actuelle
const temperature = parseFloat(prompt("Quelle est la température actuelle ?"));

// Demander à l'utilisateur s'il pleut
const pleut = confirm("Est-ce qu'il pleut ?");

// Conseiller les vêtements à porter
let conseil = "Nous vous conseillons de porter : ";

if (temperature < 5) {
  conseil += "un manteau chaud, une écharpe, des gants et un bonnet.";
} else if (temperature < 10) {
  conseil += "un manteau léger, un pull et des gants.";
} else if (temperature < 15) {
  conseil += "un pull et une veste.";
} else if (temperature < 20) {
  conseil += "un t-shirt et une veste légère.";
} else {
  conseil += "des vêtements légers.";
}

if (pleut) {
  conseil += " N'oubliez pas de prendre un parapluie ou un imperméable.";
}

alert(conseil);