function calculerPrixBillet(age) {
    if (age <= 12) {
        return 10;
    } else if (age >= 13 && age <= 17) {
        return 15;
    } else {
        return 20;
    }
}

const ageUtilisateur = parseInt(prompt("Veuillez saisir votre âge :"));
const prixBillet = calculerPrixBillet(ageUtilisateur);
console.log(`Le prix de votre billet est de ${prixBillet} $.`);
